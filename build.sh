
#echo "mkdir cache and rootfs"
#mkdir cache
#mkdir rootfs

#echo "prepare ramdisk"
#mount -t ramfs ramfs cache
#mount -t ramfs ramfs rootfs

echo "build image"
make all

#echo "upload image"
#make upload

echo "move to storagebox"
make storagebox

echo "success"
