
BASEDIR:=$(shell dab basedir)

all: info/init_ok
	dab bootstrap --minimal
	dab install zsh htop nano net-tools sudo git git-core curl lsb-release htop openssh-client wget unzip zip
	cp domainjoin.sh ${BASEDIR}/root/
	echo "en_US.UTF-8" > ${BASEDIR}/etc/locale
	echo "en_US.UTF-8 UTF-8" > ${BASEDIR}/etc/locale.gen
	dab exec dpkg-reconfigure -f noninteractive locales
	dab exec rm -rf /etc/localtime
	dab exec ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime
	dab exec echo "Europe/Berlin" > /etc/timezone
	dab exec echo '30 12 * * * rm -rf /var/cache/apt/archives/*.deb' >> /var/spool/cron/crontabs/root
	dab exec mkdir -p ~/.ssh/
	dab exec echo 'cd' >> ~/.bashrc
	dab exec apt-get remove -y postfix rpcbind openssh-server
	dab exec apt-get autoclean
	dab exec apt-get autoremove
	dab exec apt-get clean
	dab finalize

info/init_ok: dab.conf
	dab init
	touch $@

.PHONY: clean
clean:
	dab clean
	rm -f *~

.PHONY: dist-clean
dist-clean:
	dab dist-clean
	rm -f *~

.PHONY: upload
upload:
	scp -P 2222 ./*.tar.gz deploy@10.13.37.254:/mnt/storagebox/shadow/kompetenz/template/cache/

.PHONY: storagebox
storagebox:
	mv ./*.tar.gz /mnt/storagebox/template/cache
