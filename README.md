proxmox dab Ubuntu Minimal
===========================

Proxmox MakeFile to generate an lxc template

How to use
==========
####First install the dab package from proxmox:

    # On a proxmox server
    apt-get install dab
 
 
####Then clone this repository

    git clone ssh://git@gitssh.kmpt.nz:1222/core/dab-ubuntu-minimal.git
    cd dab-ubuntu-minimal
    
####Build the template
    
    make

####Last step
Once completed you should find a traball that is your new template just move it to your template/cache folder on your proxmox server.

