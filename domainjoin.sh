
# Make Univention config directory
mkdir /etc/univention

pdc=$(dig +short pdc.$(dnsdomainname))


# fetch settings
ssh -n root@$(pdc) 'ucr shell | grep -v ^hostname=' >/etc/univention/ucr_master
echo "master_ip=$pdc" >>/etc/univention/ucr_master
chmod 660 /etc/univention/ucr_master
. /etc/univention/ucr_master


# Certs, Passwort, MAC

mkdir -p /etc/univention/ssl/ucsCA/
wget --no-check-certificate -O /etc/univention/ssl/ucsCA/CAcert.pem \
   https://$(pdc)/ucs-root-ca.crt

password=$(date +%s | sha256sum | base64 | head -c 32)
hwaddress=$(ifconfig eth0 | grep -o -E '([[:xdigit:]]{1,2}:){5}[[:xdigit:]]{1,2}')
ip=$(ip addr show eth0 | grep "inet\b" | awk '{print $2}' | cut -d/ -f1)
printf '%s' "$password" >/etc/ldap.secret
chmod 0400 /etc/ldap.secret

# ldap join

ssh -n root@${ldap_master} udm computers/ubuntu create \
   --position "cn=computers,dc=huf,dc=intranet" \
   --set name=$(hostname) \
   --set mac=${hwaddress} \
   --set ip=${ip} \
   --set operatingSystem="$(lsb_release -is)" \
   --set operatingSystemVersion="$(lsb_release -rs)" \
   --set network="cn=default,cn=networks,dc=huf,dc=intranet" \
   --set password="${password}"

# ldap conf

cat >/etc/ldap/ldap.conf <<__EOF__
TLS_CACERT /etc/univention/ssl/ucsCA/CAcert.pem
URI ldap://$ldap_master:7389
BASE $ldap_base
__EOF__

cat >/etc/sssd/sssd.conf <<__EOF__
[sssd]
config_file_version = 2
reconnection_retries = 3
sbus_timeout = 30
services = nss, pam, sudo
domains = $kerberos_realm

[nss]
reconnection_retries = 3

[pam]
reconnection_retries = 3

[domain/$kerberos_realm]
auth_provider = krb5
krb5_kdcip = ${master_ip}
krb5_realm = ${kerberos_realm}
krb5_server = ${ldap_master}
krb5_kpasswd = ${ldap_master}
id_provider = ldap
ldap_uri = ldap://${ldap_master}:7389
ldap_search_base = ${ldap_base}
ldap_tls_reqcert = never
ldap_tls_cacert = /etc/univention/ssl/ucsCA/CAcert.pem
cache_credentials = true
enumerate = true
ldap_default_bind_dn = cn=$(hostname),cn=computers,${ldap_base}
ldap_default_authtok_type = password
ldap_default_authtok = $(cat /etc/ldap.secret)
__EOF__


chmod 600 /etc/sssd/sssd.conf



DEBIAN_FRONTEND=noninteractive apt-get -y install auth-client-config

cat >/etc/auth-client-config/profile.d/sss <<__EOF__
[sss]
nss_passwd=   passwd:   compat sss
nss_group=    group:    compat sss
nss_shadow=   shadow:   compat
nss_netgroup= netgroup: nis

pam_auth=
       auth [success=3 default=ignore] pam_unix.so nullok_secure try_first_pass
       auth requisite pam_succeed_if.so uid >= 500 quiet
       auth [success=1 default=ignore] pam_sss.so use_first_pass
       auth requisite pam_deny.so
       auth required pam_permit.so

pam_account=
       account required pam_unix.so
       account sufficient pam_localuser.so
       account sufficient pam_succeed_if.so uid < 500 quiet
       account [default=bad success=ok user_unknown=ignore] pam_sss.so
       account required pam_permit.so

pam_password=
       password requisite pam_pwquality.so retry=3
       password sufficient pam_unix.so obscure sha512
       password sufficient pam_sss.so use_authtok
       password required pam_deny.so

pam_session=
       session required pam_mkhomedir.so skel=/etc/skel/ umask=0077
       session optional pam_keyinit.so revoke
       session required pam_limits.so
       session [success=1 default=ignore] pam_sss.so
       session required pam_unix.so
__EOF__


auth-client-config -a -p sss
service sssd restart


# User logins

cat >/usr/share/pam-configs/ucs_mkhomedir <<__EOF__
Name: activate mkhomedir
Default: yes
Priority: 900
Session-Type: Additional
Session:
   required    pam_mkhomedir.so umask=0022 skel=/etc/skel
__EOF__

echo '*;*;*;Al0000-2400;audio,cdrom,dialout,floppy,plugdev,adm' \
  >>/etc/security/group.conf

cat >>/usr/share/pam-configs/local_groups <<__EOF__
Name: activate /etc/security/group.conf
Default: yes
Priority: 900
Auth-Type: Primary
Auth:
   required    pam_group.so use_first_pass
__EOF__

DEBIAN_FRONTEND=noninteractive pam-auth-update --force


DEBIAN_FRONTEND=noninteractive apt-get install -y heimdal-clients ntpdate


cat >/etc/krb5.conf <<__EOF__
[libdefaults]
   default_realm = $kerberos_realm
   kdc_timesync = 1
   ccache_type = 4
   forwardable = true
   proxiable = true
   default_tkt_enctypes = arcfour-hmac-md5 des-cbc-md5 des3-hmac-sha1 des-cbc-crc des-cbc-md4 des3-cbc-sha1 aes128-cts-hmac-sha1-96 aes256-cts-hmac-sha1-96
   permitted_enctypes = des3-hmac-sha1 des-cbc-crc des-cbc-md4 des-cbc-md5 des3-cbc-sha1 arcfour-hmac-md5 aes128-cts-hmac-sha1-96 aes256-cts-hmac-sha1-96
   allow_weak_crypto=true

[realms]
$kerberos_realm = {
  kdc = $master_ip $ldap_master
  admin_server = $master_ip $ldap_master
  kpasswd_server = $master_ip $ldap_master
}
__EOF__


